const app = require('./src/startup/routes');
const logger = require('./src/startup/logger');
const { connect } = require('./src/startup/database');

connect();

const port = process.env.PORT || 3030;

app.listen(port, () => logger.info(`Listening to api requests on port ${port}!`));

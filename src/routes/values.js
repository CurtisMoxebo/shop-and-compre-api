const express = require('express');
const router = express.Router();

router.get('/', (req, res) =>
  res
    .status(200)
    .send({ value1: 'value1', value2: 'value2' })
);

module.exports = router;

const express = require('express');
const { stores } = require('../enums');
const router = express.Router();

router.get('/', (req, res) => {
  res
    .status(200)
    .send(stores);
});

module.exports = router;

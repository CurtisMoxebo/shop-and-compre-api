const moment = require("moment");
const logger = require("../startup/logger");

const getIpAddress = req =>
  req.headers['x-forwarded-for'] || req.socket.remoteAddress;

const logRequestInput = (req, res, next) => {
  req.start = moment();
  const ip = getIpAddress(req);
  logger.http(`(${ip}) ${req.method} ${req.path}${req.body ? `\n${JSON.stringify(req.body)}` : ''}`);
  next();
}

const logRequestDuration = (req, res, next) => {
  const ip = getIpAddress(req);
  const duration = moment().diff(req.start);
  logger.http(`(${ip}) ${req.method} ${req.path} - Duration = ${duration}ms`);
  next();
}

module.exports = {
  logRequestInput,
  logRequestDuration
};

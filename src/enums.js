const logLevels = {
  error: 'error',     // 0
  warn: 'warn',       // 1
  info: 'info',       // 2
  http: 'http',       // 3
  verbose: 'verbose', // 4
  debug: 'debug',     // 5
  silly: 'silly'      // 6
};

const stores = [
  {
    id: 1,
    name: "Walmart",
    imageUrl: "/stores/walmart.jpg",
    url: ""
  },
  {
    id: 2,
    name: "Cosco",
    imageUrl: "/stores/cosco.png",
    url: ""
  },
  {
    id: 3,
    name: "Maxi",
    imageUrl: "/stores/maxi.png",
    url: ""
  },
  {
    id: 4,
    name: "IGA",
    imageUrl: "/stores/iga.jpg",
    url: ""
  },
  {
    id: 5,
    name: "Metro",
    imageUrl: "/stores/metro.png",
    url: ""
  },
  {
    id: 6,
    name: "Provigo",
    imageUrl: "/stores/provigo.png",
    url: ""
  }
]

module.exports = {
  logLevels,
  stores
}

const { Sequelize } = require('sequelize');
const config = require('config');
const logger = require('./logger');

const connect = () => {
  const databaseName = config.get('database.name');
  const userName = config.get('database.user');
  const password = config.get('database.password');
  const host = config.get('database.host');
  const port = config.get('database.port');

  const sequelize = new Sequelize(databaseName, userName, password, {
    host: host,
    port: port,
    dialect: 'mariadb'
  });

  sequelize
    .authenticate()
    .then(() => logger.info('Connection to the database has been established successfully!'))
    .catch(error => logger.error('Unable to connect to the database:', error));
}

module.exports = {
  connect
};

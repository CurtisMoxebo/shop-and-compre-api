const { createLogger, format, transports } = require('winston');
const config = require('config');
const moment = require('moment');

const { logLevels } = require('../enums');

const { combine, timestamp, printf } = format;
const logFormat = printf(({ level, message, _, timestamp }) => `${timestamp} ${level.toUpperCase()}: ${message}`);

const logger = createLogger({
  level: logLevels.debug,
  format: combine(
    timestamp({
      format: 'YYYY-MM-DD HH:mm:ss.SSSS'
    }),
    logFormat
  ),
  transports: [
    new transports.File({ filename: `./${config.get('logs.path')}/${moment().format('YYYY-MM-D')}.log` }),
    new transports.Console()
  ],
});

module.exports = logger;

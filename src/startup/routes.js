const express = require('express');
const app = express();
const cors = require('cors');
const config = require('config');

const {logRequestInput, logRequestDuration} = require('../middlewares/logRequest');  // Middleware to log incomming and outgoing requests
const values = require('../routes/values');
const stores = require('../routes/stores');

app.use(logRequestInput);                               // Log all incoming requests
app.use(cors({ origin: config.get('frontend.url') }));  // Enable CORS
app.use(express.json());                                // Parse json in requests' body
app.use(express.static('public'));                      // Set public folder as static
app.use(express.static('logs'));                        // Set log's folder as static
app.set('trust proxy', true);                           // Trust proxy
app.use(logRequestDuration);                            // Log all outgoing requests

/* Routes */
app.use('/values', values);
app.use('/stores', stores)

module.exports = app;
